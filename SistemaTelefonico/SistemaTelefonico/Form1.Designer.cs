﻿namespace SistemaTelefonico
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvLlamada = new System.Windows.Forms.DataGridView();
            this.Enlace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Origen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Destino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TiempoFializa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvTelefonos = new System.Windows.Forms.DataGridView();
            this.bntNuevo = new System.Windows.Forms.Button();
            this.Reloj = new System.Windows.Forms.Timer(this.components);
            this.Enlace1 = new System.Windows.Forms.Timer(this.components);
            this.lblReloj = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtOrigen = new System.Windows.Forms.TextBox();
            this.txtDestino = new System.Windows.Forms.TextBox();
            this.txtDuracion = new System.Windows.Forms.TextBox();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.lblDestino = new System.Windows.Forms.Label();
            this.lblDuracion = new System.Windows.Forms.Label();
            this.Enlace2 = new System.Windows.Forms.Timer(this.components);
            this.Enlace3 = new System.Windows.Forms.Timer(this.components);
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Disponibilidad = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLlamada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTelefonos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLlamada
            // 
            this.dgvLlamada.AllowUserToAddRows = false;
            this.dgvLlamada.AllowUserToDeleteRows = false;
            this.dgvLlamada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLlamada.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Enlace,
            this.Origen,
            this.Destino,
            this.Duracion,
            this.TiempoFializa});
            this.dgvLlamada.Location = new System.Drawing.Point(2, 41);
            this.dgvLlamada.Name = "dgvLlamada";
            this.dgvLlamada.ReadOnly = true;
            this.dgvLlamada.Size = new System.Drawing.Size(546, 168);
            this.dgvLlamada.TabIndex = 0;
            // 
            // Enlace
            // 
            this.Enlace.HeaderText = "Enlace";
            this.Enlace.Name = "Enlace";
            this.Enlace.ReadOnly = true;
            this.Enlace.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Origen
            // 
            this.Origen.HeaderText = "Origen";
            this.Origen.Name = "Origen";
            this.Origen.ReadOnly = true;
            this.Origen.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Destino
            // 
            this.Destino.HeaderText = "Destino";
            this.Destino.Name = "Destino";
            this.Destino.ReadOnly = true;
            this.Destino.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Duracion
            // 
            this.Duracion.HeaderText = "Duracion";
            this.Duracion.Name = "Duracion";
            this.Duracion.ReadOnly = true;
            this.Duracion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // TiempoFializa
            // 
            this.TiempoFializa.HeaderText = "TiempoFializa";
            this.TiempoFializa.Name = "TiempoFializa";
            this.TiempoFializa.ReadOnly = true;
            this.TiempoFializa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dgvTelefonos
            // 
            this.dgvTelefonos.AllowUserToAddRows = false;
            this.dgvTelefonos.AllowUserToDeleteRows = false;
            this.dgvTelefonos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTelefonos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nombre,
            this.Disponibilidad});
            this.dgvTelefonos.Location = new System.Drawing.Point(554, 41);
            this.dgvTelefonos.Name = "dgvTelefonos";
            this.dgvTelefonos.ReadOnly = true;
            this.dgvTelefonos.Size = new System.Drawing.Size(243, 168);
            this.dgvTelefonos.TabIndex = 1;
            // 
            // bntNuevo
            // 
            this.bntNuevo.Location = new System.Drawing.Point(2, 237);
            this.bntNuevo.Name = "bntNuevo";
            this.bntNuevo.Size = new System.Drawing.Size(63, 27);
            this.bntNuevo.TabIndex = 2;
            this.bntNuevo.Text = "Nuevo";
            this.bntNuevo.UseVisualStyleBackColor = true;
            this.bntNuevo.Click += new System.EventHandler(this.bntNuevo_Click);
            // 
            // Reloj
            // 
            this.Reloj.Enabled = true;
            this.Reloj.Interval = 250;
            this.Reloj.Tick += new System.EventHandler(this.Reloj_Tick);
            // 
            // Enlace1
            // 
            this.Enlace1.Interval = 800;
            this.Enlace1.Tick += new System.EventHandler(this.Enlace1_Tick);
            // 
            // lblReloj
            // 
            this.lblReloj.AutoSize = true;
            this.lblReloj.Location = new System.Drawing.Point(13, 11);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(0, 13);
            this.lblReloj.TabIndex = 4;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(82, 237);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(68, 26);
            this.btnGuardar.TabIndex = 5;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtOrigen
            // 
            this.txtOrigen.Location = new System.Drawing.Point(187, 244);
            this.txtOrigen.Name = "txtOrigen";
            this.txtOrigen.Size = new System.Drawing.Size(75, 20);
            this.txtOrigen.TabIndex = 6;
            // 
            // txtDestino
            // 
            this.txtDestino.Location = new System.Drawing.Point(268, 245);
            this.txtDestino.Name = "txtDestino";
            this.txtDestino.Size = new System.Drawing.Size(90, 20);
            this.txtDestino.TabIndex = 7;
            // 
            // txtDuracion
            // 
            this.txtDuracion.Location = new System.Drawing.Point(364, 245);
            this.txtDuracion.Name = "txtDuracion";
            this.txtDuracion.Size = new System.Drawing.Size(93, 20);
            this.txtDuracion.TabIndex = 8;
            // 
            // lblOrigen
            // 
            this.lblOrigen.AutoSize = true;
            this.lblOrigen.Location = new System.Drawing.Point(191, 222);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(38, 13);
            this.lblOrigen.TabIndex = 9;
            this.lblOrigen.Text = "Origen";
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(275, 222);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(43, 13);
            this.lblDestino.TabIndex = 10;
            this.lblDestino.Text = "Destino";
            // 
            // lblDuracion
            // 
            this.lblDuracion.AutoSize = true;
            this.lblDuracion.Location = new System.Drawing.Point(361, 222);
            this.lblDuracion.Name = "lblDuracion";
            this.lblDuracion.Size = new System.Drawing.Size(96, 13);
            this.lblDuracion.TabIndex = 11;
            this.lblDuracion.Text = "Duracion (HH:MM)";
            // 
            // Enlace2
            // 
            this.Enlace2.Interval = 800;
            this.Enlace2.Tick += new System.EventHandler(this.Enlace2_Tick);
            // 
            // Enlace3
            // 
            this.Enlace3.Interval = 800;
            this.Enlace3.Tick += new System.EventHandler(this.Enlace3_Tick);
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Disponibilidad
            // 
            this.Disponibilidad.HeaderText = "Disponibilidad";
            this.Disponibilidad.Name = "Disponibilidad";
            this.Disponibilidad.ReadOnly = true;
            this.Disponibilidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Disponibilidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 276);
            this.Controls.Add(this.lblDuracion);
            this.Controls.Add(this.lblDestino);
            this.Controls.Add(this.lblOrigen);
            this.Controls.Add(this.txtDuracion);
            this.Controls.Add(this.txtDestino);
            this.Controls.Add(this.txtOrigen);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblReloj);
            this.Controls.Add(this.bntNuevo);
            this.Controls.Add(this.dgvTelefonos);
            this.Controls.Add(this.dgvLlamada);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvLlamada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTelefonos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLlamada;
        private System.Windows.Forms.DataGridView dgvTelefonos;
        private System.Windows.Forms.Button bntNuevo;
        private System.Windows.Forms.Timer Reloj;
        private System.Windows.Forms.Timer Enlace1;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Enlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn Origen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Destino;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn TiempoFializa;
        private System.Windows.Forms.TextBox txtOrigen;
        private System.Windows.Forms.TextBox txtDestino;
        private System.Windows.Forms.TextBox txtDuracion;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.Label lblDuracion;
        private System.Windows.Forms.Timer Enlace2;
        private System.Windows.Forms.Timer Enlace3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Disponibilidad;
    }
}

