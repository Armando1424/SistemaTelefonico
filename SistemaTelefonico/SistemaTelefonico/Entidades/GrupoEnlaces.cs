﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaTelefonico.Entidades
{
    public class GrupoEnlaces
    {
        public int NumMaxEnlaces { get; set; }
        public int EnlacesEnUso { get; set; }
    }
}
