﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaTelefonico.Entidades
{
    public class Telefono
    {
        public string Nombre { get; set; }
        public bool Disponibilidad { get; set; }
    }
}
