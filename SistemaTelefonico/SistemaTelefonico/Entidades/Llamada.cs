﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaTelefonico.Entidades
{
    public class Llamada
    {
        public int Enlace { get; set; }
        public string Origen { get; set; }
        public string Destino { get; set; }
        public string Duracion { get; set; }
        public string TiempoFializa { get; set; }        
    }
}
