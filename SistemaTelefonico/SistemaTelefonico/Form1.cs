﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaTelefonico.Entidades;
using SistemaTelefonico.Librerias;

namespace SistemaTelefonico
{
    public partial class Form1 : Form
    {
        List<Telefono> listTelefonos = new List<Telefono>();
        List<Llamada> listLlamadas = new List<Llamada>();
        List<Timer> listTimers = new List<Timer>();
        ArmandLibrary armand = new ArmandLibrary();

        public Form1()
        {
            InitializeComponent();

            for (int i = 65; i < 73; i++)
            {
                Telefono telefono = new Telefono();
                telefono.Nombre =""+ Convert.ToChar(i);
                telefono.Disponibilidad = false;                
                listTelefonos.Add(telefono);
                dgvTelefonos.Rows.Add(telefono.Nombre, telefono.Disponibilidad);
            }

            listTimers.Add(Enlace1);
            listTimers.Add(Enlace2);
            listTimers.Add(Enlace3);

            for (int i = 0; i < 3; i++)
            {
                Llamada llamada = new Llamada();
                llamada.Enlace = i;
                llamada.Origen = string.Empty;
                llamada.Destino = string.Empty;
                llamada.Duracion = string.Empty;
                llamada.TiempoFializa = string.Empty;
                listLlamadas.Add(llamada);
                dgvLlamada.Rows.Add(llamada.Enlace, llamada.Origen, llamada.Destino, llamada.Duracion, llamada.TiempoFializa);
            }
            dgvTelefonos.Height = (listTelefonos.Count + 1) * 24;
            dgvLlamada.Height = (listLlamadas.Count + 1) * 24;           

        }

        private void ModificarTelefono(Telefono telefono, int j)
        {
            dgvTelefonos.Rows[j].Cells[1].Value = telefono.Disponibilidad;
        }

        private void ModificarLlamada(Llamada llamada, int i)
        {
            dgvLlamada.Rows[i].Cells[1].Value = llamada.Origen;
            dgvLlamada.Rows[i].Cells[2].Value = llamada.Destino;
            dgvLlamada.Rows[i].Cells[3].Value = llamada.Duracion;
            dgvLlamada.Rows[i].Cells[4].Value = llamada.TiempoFializa;
        }

        private void bntNuevo_Click(object sender, EventArgs e)
        {
            
        }

        private void Reloj_Tick(object sender, EventArgs e)
        {
            lblReloj.Text = DateTime.Now.ToLongTimeString();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {                
                bool ok1 = false, ok2 = true;
                foreach (var item in listLlamadas)
                {
                    if (item.Origen.Equals(string.Empty))
                    {
                        item.Origen = txtOrigen.Text.ToUpper();
                        item.Destino = txtDestino.Text.ToUpper();
                        item.Duracion = txtDuracion.Text;
                        item.TiempoFializa = armand.SumarTime(armand.JustNumInFec(DateTime.Now.ToShortTimeString()), txtDuracion.Text);
                        
                        for (int i = 0; i < listTelefonos.Count; i++)
                        {
                            if (listTelefonos[i].Nombre.Equals(item.Origen) || listTelefonos[i].Nombre.Equals(item.Destino))
                            {
                                if (listTelefonos[i].Disponibilidad)
                                {
                                    MessageBox.Show("El telefono: " + listTelefonos[i].Nombre + " esta ocupado");
                                    PonerDisponible(item);
                                    ok1 = false;
                                }
                                else
                                {
                                    listTelefonos[i].Disponibilidad = true;
                                    ModificarTelefono(listTelefonos[i], i);
                                    ok1 = true;
                                }
                            }
                        }
                        if (ok1)
                        {
                            ModificarLlamada(item, item.Enlace);
                            listTimers[item.Enlace].Enabled = true;
                        }
                        ok2 = false;
                        break;
                    }
                }
                if (ok2)
                {
                    MessageBox.Show("Lineas Ocupadas intentelo mas tarde");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Enlace1_Tick(object sender, EventArgs e)
        {
            dgvLlamada.Rows[0].Cells[3].Value = armand.RestarTime(dgvLlamada.Rows[0].Cells[3].Value.ToString(),"00:01");
        }

        private void Enlace2_Tick(object sender, EventArgs e)
        {
            dgvLlamada.Rows[1].Cells[3].Value = armand.RestarTime(dgvLlamada.Rows[1].Cells[3].Value.ToString(), "00:01");
        }

        private void Enlace3_Tick(object sender, EventArgs e)
        {
            dgvLlamada.Rows[2].Cells[3].Value = armand.RestarTime(dgvLlamada.Rows[2].Cells[3].Value.ToString(), "00:01");
        }

        private void PonerDisponible(Llamada item)
        {
            for (int i = 0; i < listTelefonos.Count; i++)
            {
                if (listTelefonos[i].Nombre.Equals(item.Origen) || listTelefonos[i].Nombre.Equals(item.Destino))
                {
                    listTelefonos[i].Disponibilidad = false;
                    ModificarTelefono(listTelefonos[i], i);
                }
            }
        }
    }
}
